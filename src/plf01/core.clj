(ns plf01.core)
(defn función-<-1 
  [n1 n2]
  (< n1 n2))
(defn función-<-2
  [n1 n2 n3]
  (< n1 n2 n3))
(defn función-<-3
  [n1 n2 n3 n4]
  (< n1 n2 n3 n4))

(función-<-1 5 6)
(función-<-2 5 6 1)
(función-<-3 5 6 7 8)

(defn función-<=-1
  [n1 n2]
  (<= n1 n2))
(defn función-<=-2
  [n1 n2 n3]
  (<= n1 n2 n3))
(defn función-<=-3
  [n1 n2 n3 n4]
  (<= n1 n2 n3 n4))

(función-<=-1 12 12)
(función-<=-2 16 17 17)
(función-<=-3 1000 1200 1201 1201)

(defn función-==-1
  [n1 n2]
  (== n1 n2 ))
(defn función-==-2
  [n1 n2 n3]
  (== n1 n2 n3))
(defn función-==-3
  [n1 n2 n3 n4]
  (== n1 n2 n3 n4))

(función-==-1 3 2.99999)
(función-==-2 1.0 1 1/1 )
(función-==-3 0.5 1/2 2/4 4/8)

(defn función->-1
  [n1 n2]
  (> n1 n2))
(defn función->-2
  [n1 n2 n3]
  (> n1 n2 n3))
(defn función->-3
  [n1 n2 n3 n4]
  (> n1 n2 n3 n4))

(función->-1 2 5)
(función->-2 0.75 3/4 0.5)
(función->-3 67 5 2 1)

(defn función->=-1
  [n1 n2]
  (>= n1 n2))
(defn función->=-2
  [n1 n2 n3]
  (>= n1 n2 n3))
(defn función->=-3
  [n1 n2 n3 n4]
  (>= n1 n2 n3 n4))

(función->=-1 4 5)
(función->=-2 90 2 1)
(función->=-3 12 11 13 10)

(defn funcion-assoc-1
  [vector1 índice valor]
  (assoc vector1 índice valor)
  )
(defn funcion-assoc-2
  [vector1 índice lista]
  (assoc vector1 índice lista)
  )
(defn funcion-assoc-3
 [vector1 índice valor1]
(assoc vector1 índice valor1))

(funcion-assoc-1 [1 2 4] 0 300)
(funcion-assoc-2 [10 30 "A"] 3 '( "B" "C"))
(funcion-assoc-3 [10 20 30] 2 "X")

(defn funcion-assoc-in-1
  [vector1 vector2 nuevo-valor]
  (assoc-in vector1 vector2 nuevo-valor))
(defn funcion-assoc-in-2
  [vector1 vector-indice mapa1]
  (assoc-in vector1 vector-indice mapa1))
(defn funcion-assoc-in-3
  [vector1 vector-fil-col val1]
  (assoc-in vector1 vector-fil-col val1))

(funcion-assoc-in-1 [{:Nombre "Carlos" :Edad 19}] [0 :Edad] 30)
(funcion-assoc-in-2 [{:Nombre "Carlos" :Edad 19}] [1] {:Nombre "Danny" :Edad 26})
(funcion-assoc-in-3 [[1 2 3]
                    [4 5 6]] [0 1] 9)
(defn función-concat-1
  [vector1 vector2]
  (concat vector1 vector2))
(defn función-concat-2
  [mapa1 mapa2]
  (concat mapa1 mapa2))
(defn función-concat-3
  [ls1 ls2]
  (concat ls1 ls2))

(función-concat-1 [3 4] [10 11])
(función-concat-2 {:key1 70 :key2 90} {:key3 100})
(función-concat-3 '(\A \B) '(C \D))

(defn función-conj-1
  [vector1 val1]
  (conj vector1 val1))

(defn función-conj-2
  [lista1 val1]
  (conj lista1 val1))

(defn función-conj-3
  [vector1 val1 val2 val3]
  (conj vector1 val1 val2 val3))


(función-conj-1 [12 19 20] 90)
(función-conj-2 '(6 7 8) 10)
(función-conj-3 [12 13 15] 10 11 12)

(defn función-cons-1
  [val1 lista1]
  (cons val1 lista1))
(defn función-cons-2
  [vs1 vs2]
  (cons vs1 vs2))
(defn función-cons-3
  [vs1 vs2]
  (cons vs1 vs2))


(función-cons-1 45 '(12 13 67 43))
(función-cons-2 [10 20] [30 40 50 60])
(función-cons-3 [1 2 3 4] '("A" "K1"))


(defn función-contains?-1
  [mapa llave]
  (contains? mapa llave))
(defn función-contains?-2
  [vs1 llave]
  (contains? vs1 llave))
(defn función-contains?-3
  [val1 llave]
  (contains? val1 llave ))

(función-contains?-1 {:key1 "Hola" :key2 "mundo"} :key2)
(función-contains?-2 ["a" "b" :key3] :key3)
(función-contains?-3 "Pato" 0)

(defn función-count-1
  [mapa]
  (count mapa))
(defn función-count-2
  [vs1]
  (count vs1))
(defn función-count-3
  [val1]
  (count val1))

(función-count-1 {:key1 78 :key2 false :key3 "A"})
(función-count-2 [0 1 2 3 false true "perro"])
(función-count-3 "Hola Danny")

(defn función-disj-1
  [conjunto1]
  (disj conjunto1))
(defn función-disj-2
  [conjunto1 val1]
  (disj conjunto1 val1))
(defn función-disj-3
  [conjunto1 val1 val2]
  (disj conjunto1 val1 val2))

(función-disj-1 #{9 10 11 13 14})
(función-disj-2 #{10 15 16 17} 16)
(función-disj-3 #{10 20 30 40} 20 40)

(defn función-dissoc-1
  [mapa]
  (dissoc mapa))
(defn función-dissoc-2
  [mapa llave]
  (dissoc mapa llave))
(defn función-dissoc-3
  [mapa llave1 llave2]
  (dissoc mapa llave1 llave2))

(función-dissoc-1 {90 10 20 31})
(función-dissoc-2 {:key1 60 :key2 70 :key3 90} :key3)
(función-dissoc-3 {:a 12 :b "GIT" :c "LAB" :d 1} :a :d)

(defn función-distinct-1
  [lista1]
  (distinct lista1))
(defn función-distinct-2
  [vs1]
  (distinct vs1))
(defn función-distinct-3
  [vs2]
  (distinct vs2))

(función-distinct-1 '(12 12 15 16 30 30 12 50))
(función-distinct-2 [0 1 0 1 0 0 10 2 3 4 5])
(función-distinct-3 [{} {} [] [] "A" "A" nil nil])

(defn función-distinct?-1
  [n1 n2]
  (distinct? n1 n2))
(defn función-distinct?-2
  [n1 n2 n3]
  (distinct? n1 n2 n3))
(defn función-distinct?-3
  [vs1 vs2]
  (distinct? vs1 vs2) )

(función-distinct?-1 10 50)
(función-distinct?-2 "a" "a" "c")
(función-distinct?-3 [12 15 16] [12 15 16])

(defn función-drop-last-1
  [vs1]
  (drop-last vs1))
(defn función-drop-last-2
  [val1 vs1]
  (drop-last val1 vs1))
(defn función-drop-last-3
  [val1 mapa]
  (drop-last val1 mapa))

(función-drop-last-1 [50 60 70 90 100])
(función-drop-last-2  3 [50 60 70 90 100])
(función-drop-last-3  3 {:key1 "a" :key2 "b" :key3 12 :key4 15 :key5 "c"})

(defn función-empty-1
  [lista]
  (empty lista))
(defn función-empty-2
  [vs1]
  (empty vs1))
(defn función-empty-3
  [mapa]
  (empty mapa))

(función-empty-1 '(10 "a" "Perro" [13 14]))
(función-empty-2 [12 90 100 "a" { 12 50 }])
(función-empty-3 {"GIT " "HUB"})

(defn función-empty?-1
  [lista]
  (empty? lista))
(defn función-empty?-2
  [vs1]
  (empty? vs1))
(defn función-empty?-3
  [val1]
  (empty? val1))
(función-empty?-1 ())
(función-empty?-2 ["" nil () [] {}])
(función-empty?-3 "HOLA MUNDO")

(defn función-even?-1
  [val1]
  (even? val1))
(defn función-even?-2
  [val2]
  (even? val2))
(defn función-even?-3
  [val1]
  (even? val1))

(función-even?-1 1021)
(función-even?-2 -60)
(función-even?-3 0)

(defn función-false?-1
  [val1]
  (false? val1))
(defn función-false?-2
  [val2]
  (false? val2))
(defn función-false?-3
  [val3]
  (false? val3))
(función-false?-1 false)
(función-false?-2 "False?")
(función-false?-3 nil)

(defn función-find-1
  [mapa llave]
  (find mapa llave))
(defn función-find-2
  [vector índice]
  (find vector índice))
(defn función-find-3
  [mapa llave]
  (find mapa llave))

(función-find-1 {:a 1 :b 2 :c 3 :d 5} :d)
(función-find-2 [10 20 30 70 100] 5)
(función-find-3 {:1 10 :2 20 :3 30 :4 "Hola José" [] "F"} [] )

(defn función-first-1
  [mapa]
  (first mapa))
(defn función-first-2
  [vs1]
  (first vs1))
(defn función-first-3
  [vs2]
  (first vs2))

(función-first-1 {30 40 50 60 "a" "Ary"})
(función-first-2 [])
(función-first-3 [{"Hola" "Halo Verde"} ["Cortana" "F"]])


(defn función-flatten-1
  [vs1]
  (flatten vs1))

(defn función-flatten-2
  [lista]
  (flatten lista))

(defn función-flatten-3
  [colección]
  (flatten colección))

(función-flatten-1 [20 ["F" "G" "H"] 50])
(función-flatten-2 '(30 50 ("J" "K " "L")))
(función-flatten-3 #{["HOLA"] '(1 2 3) 60 90 100})

(defn función-frequencies-1
  [vs1]
  (frequencies vs1)
  )
(defn función-frequencies-2
  [lista]
  (frequencies lista))
(defn función-frequencies-3
  [colección]
  (frequencies colección))

(función-frequencies-1 ["a" "a" "b" "c" 1 2 3 4 4 ])
(función-frequencies-2 '(1 2 3 :a :b :c :c))
(función-frequencies-3 [ [] [] [] {} {} {} #{} #{} '()])

(defn función-get-1
  [vs1 llave]
  (get vs1 llave))
(defn función-get-2
  [mapa llave]
  (get mapa llave))
(defn función-get-3
  [vs1 llave val2]
  (get vs1 llave val2))

(función-get-1 [10 40 50 12] 3)
(función-get-2 {:a "A" :b "B" :c "C"} :D)
(función-get-3 [1 2 3 4 "not"] 8 "NO SE ENCUENTRA")

(defn función-get-in-1
  [vs1 vs-fil-col]
  (get-in vs1 vs-fil-col))
(defn función-get-in-2
  [mapa vs-llave1-llave2]
  (get-in mapa vs-llave1-llave2))
(defn función-get-in-3
  [mapa vs-llave1-índice-llave2]
  (get-in mapa vs-llave1-índice-llave2))
(función-get-in-1 [["a" "b" "c"]
                   ["e" "f" "g"]]
                   [1 0])
(función-get-in-2 {:usuario "Danny" :Edad "21" :Dirección {:país "México" :Ciudad "Oaxaca"}}
                  [ :Dirección :Ciudad]
                  )
(función-get-in-3 {:escuela "ITO"
                   :carrera [{:ing "Sistemas"}
                             {:lic "Informática"}
                             ]
                   } [:carrera 0 :ing])

(defn función-into-1
  [vs1 lista1]
  (into vs1 lista1))
(defn función-into-2
  [lista1 lista2]
  (into lista1 lista2))
(defn función-into-3
  [vs1 conjunto1]
  (into vs1 conjunto1))

(función-into-1 [10 60 70] '("a" :b 3))
(función-into-2 '() '())
(función-into-3 [ [:x "X-men"] [:y "Y"] [:z "Z"] [:4 4]] #{" F" "G" "NUEVO"})

(defn función-key-1
  [mapa key1]
  (mapa key key1))
(defn función-key-2
  [mapa key2]
  (mapa key key2 ))
(defn función-key-3
  [mapa key3]
  (mapa key key3))

(función-key-1 {:k1 4 :k2 4} :k1 )
(función-key-2 {2 3} 0)
(función-key-3 {1 2 3 4 5 6} nil)

(defn función-keys-1
  [mapa]
  (keys mapa))
(defn función-keys-2
  [mapa]
  (keys mapa))
(defn función-keys-3
  [mapa]
  (keys mapa))
(función-keys-1 {:a :k1 :k2 :b})
(función-keys-2 {1 9 2 8 0 2})
(función-keys-3 {})


(defn función-max-1
  [n1]
  (max n1))
(defn función-max-2
  [n1 n2]
  (max n1 n2))
(defn función-max-3
  [n1 n2 n3]
  (max n1 n2 n3))
(función-max-1 9)
(función-max-2 12 12)
(función-max-3 1/2 0.5 2/4)

(defn función-merge-1
  [mapa1 mapa2]
  (merge mapa1 mapa2))
(defn función-merge-2
  [mapa1 mapa2 mapa3]
  (merge mapa1 mapa2 mapa3))
(defn función-merge-3
  [mapa1 mapa2 mapa3 mapa4]
  (merge mapa1 mapa2 mapa3 mapa4))

(función-merge-1 {:10 20 :30 "F" :50 "G"}{:50 "AC/DC" :10 "GUNS"})
(función-merge-2 {:A "SHE" :B "rock" :C "WANTED"} {:B "TO SEE" :F "THE STARS"} {:D "NOTHING" :E "BUT"})
(función-merge-3 {} {:A :B :10 :15} {:A "HI " :10 "AGENT 007"} {:E "MY NAME IS ..."})

(defn función-min-1
  [n1]
  (min n1))
(defn función-min-2
  [n1 n2]
  (min n1 n2))
(defn función-min-3
  [n1 n2 n3]
  (min n1 n2 n3))
(función-min-1 10)
(función-min-2 1/2 0.499999)
(función-min-3 1 1.0 1/1)

(defn función-neg?-1
  [n1]
  (neg? n1))
(defn función-neg?-2
  [n2]
  (neg? n2))
(defn función-neg?-3
  [n3]
  (neg? n3))

(función-neg?-1 -1/2)
(función-neg?-2 -0.2)
(función-neg?-3 -0)

(defn función-nil?-1
  [n1]
  (nil? n1))
(defn función-nil?-2
  [cadena1]
  (nil? cadena1))
(defn función-nil?-3
  [colección]
  (nil? colección))
(función-nil?-1 nil)
(función-nil?-2 "")
(función-nil?-3 [nil])

(defn función-not-empty-1
  [vs1]
  (not-empty vs1))
(defn función-not-empty-2
  [conjunto1]
  (not-empty conjunto1))
(defn función-not-empty-3
  [val1]
  (not-empty val1))

(función-not-empty-1 [])
(función-not-empty-2 #{})
(función-not-empty-3 "HI MASTER")

(defn función-nth-1
  [vs1 índice]
  (nth vs1 índice))
(defn función-nth-2
  [cadena1 índice]
  (nth cadena1 índice))
(defn función-nth-3
  [lista1 índice val2]
  (nth lista1 índice val2))

(función-nth-1 [true false false "01"] 3)
(función-nth-2 "ABCD" 2)
(función-nth-3 '(0 1 2) 7 "NO ENCONTRADO")

(defn función-odd?-1
  [n1]
  (odd? n1))
(defn función-odd?-2
  [n2]
  (odd? n2))

(defn función-odd?-3
  [n3]
  (odd? n3))

(función-odd?-1 9)
(función-odd?-2 912422)
(función-odd?-3 -31)

(defn función-partition-1
  [val1 vs]
  (partition val1 vs ))
(defn función-partition-2
  [val1 val2 vs2]
  (partition val1 val2 vs2))
(defn función-partition-3
  [val1 val2 val3 lista]
  (partition val1 val2 val3 lista))

(función-partition-1 1 [10 20 40 50])
(función-partition-2 2 2 [2 4 8 16])
(función-partition-3 6 3 ["a"] '(1 2 3 4 5 6 7 8 9 10 11))

(defn función-partition-all-1
  [val1 vs1]
  (partition-all val1 vs1))
(defn función-partition-all-2
  [val1 val2 vs1]
  (partition-all val1 val2 vs1))
(defn función-partition-all-3
  [val1 vs1 ]
  (partition-all val1 vs1))

(función-partition-all-1 3 ["A" "B" "C " 1 3 2 5])
(función-partition-all-2 2 1 [90 10 200 100 :k :a :b])
(función-partition-all-3   5 [1 2 3 4 5])

(defn función-peek-1
  [vs1]
  (peek vs1))
(defn función-peek-2
  [lista]
  (peek lista))
(defn función-peek-3
  [lista]
  (peek lista))
(función-peek-1 [1 181 12 "A" "CAR" {1 1 2 2}])
(función-peek-2 '())
(función-peek-3 '("HOLA" "HI" "KONICHIWA"))

(defn función-pop-1
  [vs1]
  (pop vs1))
(defn función-pop-2
  [lista]
  (pop lista))
(defn función-pop-3
  [vs1]
  (pop vs1))
(función-pop-1 ["X " " Y" "Z" ])
(función-pop-2 '("NO ES LLAVE" :k1 :k2 :k3 ))
(función-pop-3 [nil])

(defn función-pos-1
  [val1 ]
  (pos? val1))
(defn función-pos-2
  [val2 ]
  (pos? val2))
(defn función-pos-3
  [val1]
  (pos? val1))

(función-pos-1 0.0000001)
(función-pos-2 0)
(función-pos-3 -0.1)

(defn función-quot-1
  [n1 divisor]
  (quot n1 divisor))
(defn función-quot-2
  [n2 divisor2]
  (quot n2 divisor2))
(defn función-quot-3
  [n3 divisor]
  (quot n3 divisor))

(función-quot-1 23.90 7)
(función-quot-2 7 5)
(función-quot-3 100 1.5)

(defn función-range-1
  [rango]
  (range rango))
(defn función-range-2
  [rangoInicial rangoFinal]
  (range rangoInicial rangoFinal))
(defn función-range-3
  [rangoInicial rangoFinal distancia]
  (range rangoInicial rangoFinal distancia))

(función-range-1 50)
(función-range-2 20 60)
(función-range-3 0 1000 50)

(defn función-rem-1
  [n1 divisor]
  (rem n1 divisor))
(defn función-rem-2
  [n2 divisor2]
  (rem n2 divisor2))
(defn función-rem-3
  [n3 divisor]
  (rem n3 divisor))

(función-rem-1 -50 -4)
(función-rem-2 1/2 1/5)
(función-rem-3 1001 24)

(defn función-repeat-1
  [n1 val1]
  (repeat n1 val1))
(defn función-repeat-2
  [n1 vs1]
  (repeat n1 vs1))
(defn función-repeat-3
  [n3 lista]
  (repeat n3 lista))

(función-repeat-1 3 "MEXICO !")
(función-repeat-2 10 [" 1" true false "2 " :k \A])
(función-repeat-3 3 '(10 20))

(defn función-replace-1
  [palabra remplazo]
  (replace palabra remplazo))
(defn función-replace-2
  [ vs1 vs2]
  (replace  vs1 vs2))
(defn función-replace-3
  [vs1 mapa]
  (replace vs1 mapa))

(función-replace-1 ["HOLA DANY"] { "HOLA" "JOSÉ"})
(función-replace-2 [] [1 2 3])
(función-replace-3 [" f" "G"] {:k1 "F1"})

(defn función-rest-1
  [vs1]
  (rest vs1))
(defn función-rest-2
  [lista]
  (rest lista))
(defn función-rest-3
  [conjunto1]
  (rest conjunto1))
(función-rest-1 [10 "A" "B" "X" "Y" "Z"])
(función-rest-2 '(:A))
(función-rest-3 #{ 90 100 "F" "En" "El chat"})

(defn función-select-keys-1
  [mapa llave1]
  (select-keys mapa llave1))
(defn función-select-keys-2
  [mapa vs-llave1-llave2]
  (select-keys mapa vs-llave1-llave2))
(defn función-select-keys-3
  [vs1 vs2]
  (select-keys vs1 vs2))
(función-select-keys-1 {:k1 "F " :k2 "EN " :k10 "EL CHAT"} [:k1])
(función-select-keys-2 {:k1 "F " :k2 "EN " :k10 "EL CHAT"} [:k1 :k10])
(función-select-keys-3 [30 40 10 01 02] [0 1 2])

(defn función-shuffle-1
  [lista]
  (shuffle lista))
(defn función-shuffle-2
  [ vs2 ]
  ( shuffle vs2))
(defn función-shuffle-3
  [lista2]
  (shuffle lista2))

(función-shuffle-1 '(9 10 "A" "B" true))
(función-shuffle-2 [90 19 :g :f "A"])
(función-shuffle-3 '(" Do" "you " "wanna" "know?"))

(defn función-sort-1
  [vs1]
  (sort vs1))
(defn función-sort-2
  [lista]
  (sort lista ))
(defn función-sort-3
  [conjunto1]
  (sort conjunto1))

(función-sort-1 [9 10 1.2 12 32 12])
(función-sort-2 '( 1 1/2 1/4 1/8 9/2))
(función-sort-3 #{90 10 100 109 91 1 5})

(defn función-split-at-1
  [n vs1]
  (split-at n vs1))
(defn función-split-at-2
  [n mapa]
  (split-at n mapa))
(defn función-split-at-3
  [n2 lista]
  (split-at n2 lista))

(función-split-at-1 2 [8 9 10 "A" "B" "C"])
(función-split-at-2 2 {:k1 8 :k2 "Alpha" :k3 "Beta" :k4 "Vegeta"})
(función-split-at-3  1 '(30 30 40 40))

(defn función-str-1
  []
  (str))
(defn función-str-2
  [cadena1]
  (str cadena1))
(defn función-str-3
  [val1 val2 val3]
  (str val1 val2 val3))
(función-str-1)
(función-str-2 "Shodow Verse")
(función-str-3  1 " by two" " by three")

(defn función-subs-1
  [cadena1 indice]
  (subs cadena1  indice))
(defn función-subs-2
  [cadena1 indice-incio indice-final]
  (subs cadena1  indice-incio indice-final))
(defn función-subs-3
  [cadena2 indice2]
  (subs cadena2 indice2))

(función-subs-1 "HOLA  MUNDO" 5)
(función-subs-2 "HI MASTER CHIEF HERE IS'NT CORTANA" 27 34)
(función-subs-3 "BY NAME" 2)

(defn función-subvec-1
  [vs1 indice]
  (subvec vs1 indice ))
(defn función-subvec-2
  [vs1 indice-incio indice-final]
  (subvec vs1 indice-incio indice-final))
(defn función-subvec-3
  [vs2 indice2]
  (subvec vs2 indice2))

(función-subvec-1 ["A" "B" "3" 4] 2)
(función-subvec-2 ["HELLO" 1 1 7] 1 4)
(función-subvec-3 [nil 1] 1)

(defn función-take-1
  [n conjunto1]
  (take n conjunto1))
(defn función-take-2
  [n lista]
  (take n lista))
(defn función-take-3
  [n vs1]
  (take n vs1))

(función-take-1 1 #{1 2 90 "a"})
(función-take-2 2 '({} {} [] []))
(función-take-3 3 [90 10 23 1])

(defn función-true?-1
  [val1]
  (true? val1))
(defn función-true?-2
  [vs1]
  (true? vs1))
(defn función-true?-3
  [val2]
  (true? val2))

(función-true?-1 1)
(función-true?-2 [false true false 5])
(función-true?-3 true)

(defn función-val-1
  [mapa]
  (map val mapa ))
(defn función-val-2
  [mapa]
  (map val mapa))
(defn función-val-3
  [ mapa2]
  (map val mapa2))

(función-val-1 {:k1 "f1" :k2 "fifa2" :k8 "L8"})
(función-val-2 {1 2 29 39 40 50})
(función-val-3 {:k1 1 :k2 4} )

(defn función-vals-1
  [mapa]
  (vals mapa))
(defn función-vals-2
  [mapa2]
  (vals mapa2))
(defn función-vals-3
  [mapa]
  (vals mapa))

(función-vals-1 {{1 2} {3 4} [5 6] [7 8] })
(función-vals-2 {20 30 60 70 "A" "B"})
(función-vals-3 {})

(defn función-zero?-1
  [n1]
  (zero? n1))
(defn función-zero?-2
  [n2 ]
  (zero? n2))
(defn función-zero?-3
  [n3]
  (zero? n3))
(función-zero?-1 0.000001)
(función-zero?-2 (/ 10 10 ))
(función-zero?-3 0x00000000000)

(defn función-zipmap-1
  [llaves valores]
  (zipmap llaves valores))
(defn función-zipmap-2
  [llaves valores2]
  (zipmap llaves valores2))
(defn función-zipmap-3
  [llaves valores3]
  (zipmap llaves valores3))

(función-zipmap-1 [:K1 :K2 :K9] [1 2 3 4])
(función-zipmap-2 [:K1 :JK :KL :KM :KZ] [10 20 40])
(función-zipmap-3 [:k1] [2 4 6 8])